import java.awt.*;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        generateRandomTree(400,0.01,50,0,1400,1400);
    }

    public static void affichageMenu()
    {
        System.out.print("/***  Initialisation  ***/ ");
        System.out.print("Entrer nbFeuilles : ");
        int nbFeuilles;
        Scanner scanner1 = new Scanner(System.in);
        nbFeuilles = scanner1.nextInt();

        System.out.print("Entrer proportionCoupe : ");
        int proportionCoupe;
        Scanner scanner2 = new Scanner(System.in);
        proportionCoupe = scanner2.nextInt();

        System.out.print("Entrer minDimensionCoupe : ");
        int minDimensionCoupe;
        Scanner scanner3 = new Scanner(System.in);
        minDimensionCoupe = scanner3.nextInt();

        System.out.print("Entrer memeCouleurProb : ");
        double memeCouleurProb;
        Scanner scanner4 = new Scanner(System.in);
        memeCouleurProb = scanner4.nextDouble();

        System.out.print("Entrer largeur : ");
        int largeur;
        Scanner scanner5 = new Scanner(System.in);
        largeur = scanner5.nextInt();

        System.out.print("Entrer hauteur : ");
        int hauteur;
        Scanner scanner6 = new Scanner(System.in);
        hauteur = scanner6.nextInt();
    }
    public static void generateRandomTree(int nbFeuille, double proportionCoupe, int minDimensionCoupe, double memeCouleurProb,int largeur,int hauteur) throws IOException {
        int[] coord = new int[]{0,0};
        Noeud test = new Noeud(coord,largeur,hauteur,Color.WHITE,(largeur*hauteur) / Math.pow((largeur+hauteur),1.5));
        test.axe='X';
        test.coordDecoupe=0;
        Kdtree tree = new Kdtree(test,1);
        division(tree,nbFeuille,proportionCoupe,minDimensionCoupe,memeCouleurProb);
        Image img = new Image(largeur,hauteur);
        setRect(img,tree.root);
        img.save("test.png");
    }
    public static void setRect(Image image,Noeud noeud)
    {
        if(!noeud.isLeaf())
        {
            setRect(image,noeud.leftChild);
            setRect(image,noeud.rightChild);
        }
        else
        {
            image.setRectangle(noeud.coordStart[0],(noeud.coordStart[0]+ noeud.largeur),noeud.coordStart[1],(noeud.coordStart[1]+ noeud.hauteur),noeud.color);
        }
    }
    public static boolean division(Kdtree kdtree, int nbFeuilles, double proportionCoupe, int minDimensionCoupe, double memeCouleurProb)
    {
        Noeud noeudDivise = chooseLeft(kdtree.root);
        if(noeudDivise.largeur<minDimensionCoupe||noeudDivise.hauteur<minDimensionCoupe)
        {
            return false;
        }
        else
        {
            if(kdtree.nbFeuilles>=nbFeuilles)
            {
                return false;
            }
            else
            {
                kdtree.nbFeuilles += 1;
                divisionNoeud(noeudDivise,proportionCoupe,memeCouleurProb);
                return !division(kdtree, nbFeuilles, proportionCoupe, minDimensionCoupe, memeCouleurProb);
            }
        }
    }
    public static void divisionNoeud(Noeud racine, double proportionCoupe, double memeCouleurProb)
    {
        if(chooseAxeDivision(racine) == 'X'){
            int coordDivision = chooseCoordDivision(proportionCoupe,racine.largeur);
            int[] newCoord;
            int newLargeur;
            int newHauteur = racine.hauteur;
            racine.axe= 'X';
            racine.coordDecoupe=coordDivision;

            newCoord= new int[]{racine.coordStart[0],racine.coordStart[1]};
            newLargeur = calculLargeur(racine,'L');
            racine.leftChild=new Noeud(newCoord,newLargeur,newHauteur,chooseColor(racine.color,memeCouleurProb),calculPoid(newLargeur,newHauteur));

            newCoord= new int[]{coordDivision,racine.coordStart[1]};
            newLargeur = calculLargeur(racine,'R');
            racine.rightChild=new Noeud(newCoord,newLargeur, newHauteur,chooseColor(racine.color,memeCouleurProb),calculPoid(newLargeur,newHauteur));
        }
        else{
            int coordDivision = chooseCoordDivision(proportionCoupe,racine.hauteur);
            int[] newCoord;
            int newLargeur = racine.largeur;
            int newHauteur;
            racine.axe= 'Y';
            racine.coordDecoupe=coordDivision;
            newCoord = new int[]{racine.coordStart[0],racine.coordStart[1]};
            newHauteur = calculHauteur(racine,'L');
            racine.leftChild=new Noeud(newCoord,racine.largeur,newHauteur,chooseColor(racine.color,memeCouleurProb),calculPoid(newLargeur,newHauteur));

            newCoord = new int[]{racine.coordStart[0],coordDivision};
            newHauteur = calculHauteur(racine,'R');
            racine.rightChild=new Noeud(newCoord,racine.largeur,newHauteur,chooseColor(racine.color,memeCouleurProb),calculPoid(newLargeur,newHauteur));
        }
    }
    public static int calculLargeur(Noeud noeudParent, char positionChild)
    {
        if(noeudParent.axe=='X')
        {
            if(positionChild=='L'){
                return noeudParent.coordDecoupe-noeudParent.coordStart[0];
            } else if (positionChild == 'R') {
                return noeudParent.largeur - noeudParent.coordDecoupe + noeudParent.coordStart[0];
            }
        } else if (noeudParent.axe == 'Y') {
            return noeudParent.largeur;
        }
        return -1;
    }

    public static int calculHauteur(Noeud noeudParent, char positionChild)
    {
        if(noeudParent.axe=='X')
        {
            return noeudParent.hauteur;
        } else if (noeudParent.axe == 'Y') {
            if(positionChild=='L'){
                return noeudParent.coordDecoupe - noeudParent.coordStart[1];
            } else if (positionChild == 'R') {
                return noeudParent.coordStart[1] + noeudParent.hauteur - noeudParent.coordDecoupe;
            }
        }
        return -1;
    }
    public static double calculPoid(int largeur,int hauteur)
    {
        return (largeur*hauteur) / Math.pow((largeur+hauteur),1.5);
    }
    public static char chooseAxeDivision(Noeud noeud) {
        double probX = (double)noeud.largeur / (double)(noeud.largeur + noeud.hauteur);
        double r = Math.random();
        if (r < probX) {
            return 'X';
        } else {
            return 'Y';
        }
    }
    public static Noeud chooseLeft(Noeud noeud){
        if(!noeud.isLeaf())
        {
            Noeud noeudLeft = chooseLeft(noeud.leftChild);
            Noeud noeudRight = chooseLeft(noeud.rightChild);
            if(noeudLeft.poid>noeudRight.poid){
                return noeudLeft;
            }
            else
            {
                return noeudRight;
            }
        }
        else
        {
            return noeud;
        }
    }
    public static int chooseCoordDivision(double proportionCoupe, int tailleDimensionDecoupe)
    {
        Random random = new Random();
        int low = (int) (tailleDimensionDecoupe*proportionCoupe);
        int high = (int) (tailleDimensionDecoupe*(1-proportionCoupe));
        return random.nextInt( high - low ) + low;
    }

    public static Color chooseColor(Color colorParent, double memeCouleurProb) {
        double prob = Math.random();
        Random random = new Random();
        if (prob < memeCouleurProb) {
            return colorParent;
        } else {
            Color[] arrayColor = new Color[]{Color.red, Color.blue, Color.yellow, Color.black, Color.white};
            Color[] colorLeft = new Color[arrayColor.length - 1];
            for (int i = 0, j = 0; i < arrayColor.length; i++) {
                if (arrayColor[i] != colorParent) {
                    colorLeft[j++] = arrayColor[i];
                }
            }
            return colorLeft[random.nextInt(4)];
        }
    }

}